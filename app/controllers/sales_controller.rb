class SalesController < ApplicationController
  before_action :authenticate_user!
  
  def new
    @sale = Sale.new
    
    preference_data = Hash.new
    
    sale_order = SaleOrder.get_active(current_user)
    
    preference_data[:items] = Array.new
    
    sale_order.items.each do |item|
      new_item = {
        title: "Sale Demo",
        quantity: item.quantity,
        unit_price: item.product.price,
        currency_id: "ARS"
      }
      preference_data[:items] << new_item
    end
    
    preference_data[:payer] = {
      email: current_user["email"]
    }
    
    @url = "http://#{request.host_with_port}/sales/#{sale_order.id}/callback"
    
    preference_data[:back_urls] = {
      success: "#{request.host_with_port}/sales/#{sale_order.id}/callback",
      failure: "#{request.host_with_port}/sales/#{sale_order.id}/fail",
      pending: "#{request.host_with_port}/sales/#{sale_order.id}/pending"
    }
    
    @preference = $mp.create_preference(preference_data)
    
  end
  
  def pending
    @sale_order = SaleOrder.find(params[:id])
    @sale_order.finish
    @status = "La compra se ha procesado, recibira una confirmacion via email en las proximas horas"
    render :callback
  end
  
  def callback
    @sale_order = SaleOrder.find(params[:id])
    @sale_order.finish
    @status = "La compra se ha procesado con exito!"
    render :callback
  end
  
  def fail
    @sale_order = SaleOrder.find(params[:id])
    @sale_order.cancel!
    redirect_to "/products"
  end
   

end
