class SessionsController < Devise::SessionsController
  
  def create
    super  
    customer = $mp.get("/v1/customers/search", {"email" => current_user["email"]})
    
    if customer["status"] != "200" 
      customer = ($mp.post("/v1/customers", {"email" => current_user["email"]}))  
    end
    
    $customer = customer["response"]["results"][0]
    
  end
end
