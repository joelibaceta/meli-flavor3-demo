class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :user_id
      t.integer :sale_order_id
      t.timestamps null: false
    end
  end
end
