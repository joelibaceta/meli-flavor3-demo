class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :sale_order_id
      t.integer :product_id

      t.timestamps null: false
    end
  end
end
