class CreateSaleOrders < ActiveRecord::Migration
  def change
    create_table :sale_orders do |t|
      t.string :state
      t.timestamps null: false
    end
  end
end
