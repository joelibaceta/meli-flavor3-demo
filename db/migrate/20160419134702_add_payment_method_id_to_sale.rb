class AddPaymentMethodIdToSale < ActiveRecord::Migration
  def change
    add_column :sales, :payment_method_id, :string
  end
end
