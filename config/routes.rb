Rails.application.routes.draw do
  
  devise_for :users, :controllers => {:sessions => 'sessions'}
  
  get 'items/new'

  get 'items/index'

  get 'items/show'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self) 
  
  root 'products#index'
  
  resources :items
  
  resources :sales do 
    member do 
      get :pending
      get :fail
      get :callback
    end
  end
  
  resources :sale_orders
  
  resources :products
 
end
